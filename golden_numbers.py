import argparse
import re
import csv

def get_arguments():
    """Simple function to get the arguments from the user. The argument represents the prefix for the msisdn range"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--prefix', type=str, required=True, help = 'MSISDN range prefix to be analysed')
    args = parser.parse_args()
    return args.prefix

#def get_last_digit(number):
#    return int(number)%10

#def generate_special_pattern_file():
#    with open('special_regex_deffinition','r') as f:
#        data = f.read()
#    generated_data = data.replace('[6]',f'[{str(get_last_digit(get_arguments()))}]')
#    with open('generated_special_regex_deffinition','w') as file:
#        file.write(generated_data)

class pattern_dictionary_generator:
    def __init__(self,file):
        self.pattern_dict={}
        with open(file,'r') as f:
            data = f.readlines()
        for line in data:
            self.pattern_dict[line.split('=')[0]] = line.split('=')[1].replace('\n','')

class range_generator:
    """Class that instatiates objects that represent a range of msisdn created by starting fom a prefix representinc mcc+mnc...."""
    def __init__(self,prefix):
        self.prefix = prefix
    def generate_range(self):
        self.range_of_msisdns = [self.prefix + '0' * (5-len(str(suffix))) + str(suffix) for suffix in range(0,100000)]
        return self.range_of_msisdns

class category_validator(range_generator):
    """Class that validates regex patterns"""
    def __init__(self,prefix,pattern):
        super().__init__(prefix)
        self.pattern = pattern
    def validate_pattern(self):
        self.list_of_patterns = []
        for msisdn in self.range_of_msisdns:
            for regex in list(self.pattern.items()):
               if bool(re.search(regex[1], msisdn)):
                   self.list_of_patterns.append((msisdn,regex[0]))
                   break

my_dict = pattern_dictionary_generator("./regex_definition")
my_range = category_validator(get_arguments(),my_dict.pattern_dict)
my_range.generate_range()
my_range.validate_pattern()
validated_list_my_range = set([item[0] for item in my_range.list_of_patterns])
total_list = set(my_range.range_of_msisdns)
remaining_list = total_list - (validated_list_my_range)

with open(f'report{get_arguments()} ','a') as f:
    writer = csv.writer(f)
    writer.writerow(('MSISDN','Validated Pattern'))
    for line in my_range.list_of_patterns:
            writer.writerow(line)

with open(f'report_remaining{get_arguments()} ','a') as f:
    writer = csv.writer(f)
    writer.writerow(('MSISDN','Validated Pattern'))
    for item in (list(remaining_list)):
        writer.writerow((item,'No Pattern_validated'))
